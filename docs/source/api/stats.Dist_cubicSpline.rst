Dist_cubicSpline
================

.. currentmodule:: stats

.. autoclass:: Dist_cubicSpline
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Dist_cubicSpline.F_tot
      ~Dist_cubicSpline.cdf
      ~Dist_cubicSpline.f
      ~Dist_cubicSpline.name
      ~Dist_cubicSpline.pdf
      ~Dist_cubicSpline.ppf
      ~Dist_cubicSpline.rvs
      ~Dist_cubicSpline.rvs_xy

   .. rubric:: Methods Documentation

   .. automethod:: F_tot
   .. automethod:: cdf
   .. automethod:: f
   .. automethod:: name
   .. automethod:: pdf
   .. automethod:: ppf
   .. automethod:: rvs
   .. automethod:: rvs_xy
