Dist_reject
===========

.. currentmodule:: stats

.. autoclass:: Dist_reject
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Dist_reject.F_tot
      ~Dist_reject.cdf
      ~Dist_reject.f
      ~Dist_reject.f_underlying
      ~Dist_reject.name
      ~Dist_reject.pdf
      ~Dist_reject.ppf
      ~Dist_reject.rvs
      ~Dist_reject.rvs_xy

   .. rubric:: Methods Documentation

   .. automethod:: F_tot
   .. automethod:: cdf
   .. automethod:: f
   .. automethod:: f_underlying
   .. automethod:: name
   .. automethod:: pdf
   .. automethod:: ppf
   .. automethod:: rvs
   .. automethod:: rvs_xy
