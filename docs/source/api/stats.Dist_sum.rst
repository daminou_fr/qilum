Dist_sum
========

.. currentmodule:: stats

.. autoclass:: Dist_sum
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Dist_sum.F_tot
      ~Dist_sum.cdf
      ~Dist_sum.f
      ~Dist_sum.name
      ~Dist_sum.pdf
      ~Dist_sum.ppf
      ~Dist_sum.rvs
      ~Dist_sum.rvs_xy

   .. rubric:: Methods Documentation

   .. automethod:: F_tot
   .. automethod:: cdf
   .. automethod:: f
   .. automethod:: name
   .. automethod:: pdf
   .. automethod:: ppf
   .. automethod:: rvs
   .. automethod:: rvs_xy
