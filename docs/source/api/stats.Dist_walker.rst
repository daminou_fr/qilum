Dist_walker
===========

.. currentmodule:: stats

.. autoclass:: Dist_walker
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Dist_walker.F_tot
      ~Dist_walker.cdf
      ~Dist_walker.f
      ~Dist_walker.name
      ~Dist_walker.pdf
      ~Dist_walker.pmf
      ~Dist_walker.ppf
      ~Dist_walker.rvs

   .. rubric:: Methods Documentation

   .. automethod:: F_tot
   .. automethod:: cdf
   .. automethod:: f
   .. automethod:: name
   .. automethod:: pdf
   .. automethod:: pmf
   .. automethod:: ppf
   .. automethod:: rvs
