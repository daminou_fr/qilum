Dist_scale
==========

.. currentmodule:: stats

.. autoclass:: Dist_scale
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~Dist_scale.F_tot
      ~Dist_scale.cdf
      ~Dist_scale.f
      ~Dist_scale.name
      ~Dist_scale.pdf
      ~Dist_scale.ppf
      ~Dist_scale.rvs
      ~Dist_scale.rvs_xy

   .. rubric:: Methods Documentation

   .. automethod:: F_tot
   .. automethod:: cdf
   .. automethod:: f
   .. automethod:: name
   .. automethod:: pdf
   .. automethod:: ppf
   .. automethod:: rvs
   .. automethod:: rvs_xy
